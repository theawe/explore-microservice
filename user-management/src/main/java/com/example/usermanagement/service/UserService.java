package com.example.usermanagement.service;

//import com.example.usermanagement.models.User;
import com.example.usermanagement.dtos.RequestCreateUser;
import com.example.usermanagement.dtos.UserDto;
import com.example.usermanagement.exception.NotFoundException;
import com.example.usermanagement.models.Role;
import com.example.usermanagement.models.User;
import com.example.usermanagement.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    ModelMapper modelMapper = new ModelMapper();
    public List<UserDto> getAll(){
        List<UserDto> userDtoList = new ArrayList<>();
        List<User> userList = userRepository.findAll();
        for (User user:
             userList) {
            UserDto userDto = modelMapper.map(user, UserDto.class);
            userDtoList.add(userDto);
        }
        return  userDtoList;
    }

    public UserDto getById(Long id){
        User user = userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("No user by ID: " + id));
        return modelMapper.map(user,UserDto.class);
    }

    public  UserDto createUser(RequestCreateUser rUser){
        User user = getUserDto(rUser);
        user = userRepository.save(user);
        return modelMapper.map(user, UserDto.class);
    }

   public UserDto updateUser(RequestCreateUser rUser,Long id){
        User user =  userRepository.findById(id)
                .orElseThrow(() ->new NotFoundException("No user by ID: " + id));
//        rUser.setIdUser(id);
        User newUser = getUserDto(rUser);
        newUser.setIdUser(id);
        newUser = userRepository.save(newUser);

       return modelMapper.map(newUser, UserDto.class);
    }

    public String deleteUser(Long id,Long idAdmin){
        User user = userRepository.findById(id).orElseThrow(
                () -> new NotFoundException("No user by ID: " + id)
        );
        User userAdmin = userRepository.findById(idAdmin).orElseThrow(
                () -> new NotFoundException("No user by ID: " + id)
        );

        if(userAdmin.getRole().getRoleType().getIdRoleType() != 1){
            throw new NotFoundException("Id is not admin ID: "+ userAdmin.getIdUser());
        }

        userRepository.delete(user);
        return null;
    }


    private User getUserDto(RequestCreateUser rUser) {
        User adminUser = userRepository.findById(rUser.getCreatedId()).orElseThrow(
                () -> new NotFoundException("No user by ID: " + rUser.getCreatedId())
        );
        if(adminUser.getRole().getRoleType().getIdRoleType() != 1){
            throw new NotFoundException("Id is not admin ID: "+ rUser.getCreatedId());
        }
        User user = new User();

        user.setEmail(rUser.getEmail());
        user.setPassword(rUser.getPassword());
        user.setRole(new Role(rUser.getRoleId()));
        return user;
//        user = userRepository.save(user);
//        return modelMapper.map(user, UserDto.class);
    }


}
