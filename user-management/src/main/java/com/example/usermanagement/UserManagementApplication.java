package com.example.usermanagement;

import com.example.usermanagement.event.LaporanCreatedEvent;
import org.slf4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.kafka.annotation.KafkaListener;

@SpringBootApplication
@EnableEurekaClient
public class UserManagementApplication {
	public static void main(String[] args) {
		SpringApplication.run(UserManagementApplication.class, args);
	}

	@KafkaListener(topics = "laporanTopic")
	public void handleLaporan(LaporanCreatedEvent laporanCreatedEvent){
		//Do Something about this massage
		System.out.println("Laporan Topic");
		System.out.println(laporanCreatedEvent.getId());
	}
}
