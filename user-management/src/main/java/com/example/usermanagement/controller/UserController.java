package com.example.usermanagement.controller;

import com.example.usermanagement.dtos.RequestCreateUser;
import com.example.usermanagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    UserService userService;

    @PostMapping
    public ResponseEntity<Object> createUser(@RequestBody RequestCreateUser requestCreateUser) {
        return new ResponseEntity<Object>(userService.createUser(requestCreateUser), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<Object> getAllUser(){
        return new ResponseEntity<>(userService.getAll(),HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable(value = "id") Long id){
        return new ResponseEntity<>(userService.getById(id),HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable(value = "id") Long id,@RequestBody RequestCreateUser requestCreateUser){
        return new ResponseEntity<>(userService.updateUser(requestCreateUser,id),HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable(value = "id")Long id, @RequestBody RequestCreateUser requestCreateUser){
        return new ResponseEntity<>((userService.deleteUser(id,requestCreateUser.getCreatedId())),HttpStatus.OK);
    }

}
