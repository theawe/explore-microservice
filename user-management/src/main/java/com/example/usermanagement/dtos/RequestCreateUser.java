package com.example.usermanagement.dtos;

public class RequestCreateUser {

    private Long idUser;
    private Long roleId;
    private String email;
    private String password;
    private Long createdId;

    public RequestCreateUser() {
    }

    public RequestCreateUser(Long idUser, Long roleId, String email, String password, Long createdId) {
        this.idUser = idUser;
        this.roleId = roleId;
        this.email = email;
        this.password = password;
        this.createdId = createdId;
    }

    public RequestCreateUser(Long roleId, String email, String password, Long createdId) {
        this.roleId = roleId;
        this.email = email;
        this.password = password;
        this.createdId = createdId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getCreatedId() {
        return createdId;
    }

    public void setCreatedId(Long createdId) {
        this.createdId = createdId;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }
}
