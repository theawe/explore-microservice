package com.example.usermanagement.dtos;

public class RoleTypeDto {

    private long idRoleType;
    private String roleTypeName;

    public RoleTypeDto() {
    }

    public RoleTypeDto(long idRoleType, String roleTypeName) {
        this.idRoleType = idRoleType;
        this.roleTypeName = roleTypeName;
    }

    public long getIdRoleType() {
        return idRoleType;
    }

    public void setIdRoleType(long idRoleType) {
        this.idRoleType = idRoleType;
    }

    public String getRoleTypeName() {
        return roleTypeName;
    }

    public void setRoleTypeName(String roleTypeName) {
        this.roleTypeName = roleTypeName;
    }
}
