package com.example.gateway.models;


public enum ERole {
    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN,

    ROLE_MAKER,
    ROLE_APPROVER,
    ROLE_CHECKER
}
