package com.example.applaporanpajak.event;

public class LaporanCreatedEvent {

    private Long id;

    public LaporanCreatedEvent() {
    }

    public LaporanCreatedEvent(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
