package com.example.applaporanpajak.service;

import com.example.applaporanpajak.dtos.LaporanPajakDto;
import com.example.applaporanpajak.dtos.RequestCheckLaporan;
import com.example.applaporanpajak.dtos.RequestCreateLaporan;
import com.example.applaporanpajak.event.LaporanCreatedEvent;
import com.example.applaporanpajak.exception.NotFoundException;
import com.example.applaporanpajak.models.LaporanPajak;
import com.example.applaporanpajak.models.User;
import com.example.applaporanpajak.repository.LaporanPajakRepository;
import com.example.applaporanpajak.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LaporanPajakService {
    @Autowired
    LaporanPajakRepository laporanPajakRepository;

    @Autowired
    UserRepository userRepository;

    private final KafkaTemplate<String, LaporanCreatedEvent> kafkaTemplate;

    ModelMapper modelMapper = new ModelMapper();

    public LaporanPajakService(KafkaTemplate<String, LaporanCreatedEvent> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public List<LaporanPajakDto> getAllLaporan(){
        List<LaporanPajakDto> laporanPajakDtos = new ArrayList<>();
        List<LaporanPajak> laporanPajaks = laporanPajakRepository.findAll();
        for (LaporanPajak laporan :
                laporanPajaks) {
            LaporanPajakDto laporanPajakDto = modelMapper.map(laporan,LaporanPajakDto.class);
            laporanPajakDtos.add(laporanPajakDto);

        }
        return  laporanPajakDtos;
    }

    public LaporanPajakDto createLaporan(RequestCreateLaporan rLaporan){
        //Check user made
        User userMade =userRepository.findById(rLaporan.getIdMadeBy()).orElseThrow(
                () -> new NotFoundException("Id not found : " + rLaporan.getIdMadeBy())
        );
        if(!userMade.getRole().getName().equalsIgnoreCase("maker")){
            throw new NotFoundException("Id bukan maker ");
        }
        LaporanPajak laporanPajak = new LaporanPajak();
        laporanPajak.setCreatedDate(rLaporan.getCreatedDate());
        laporanPajak.setStatus(rLaporan.getStatus());
        laporanPajak.setNomorResi(rLaporan.getNomorResi());
        laporanPajak.setUserByMadeBy(new User(rLaporan.getIdMadeBy()));
        laporanPajakRepository.save(laporanPajak);
        kafkaTemplate.send("laporanTopic",new LaporanCreatedEvent(laporanPajak.getNomorResi()));
        return modelMapper.map(laporanPajak,LaporanPajakDto.class);
    }

    public LaporanPajakDto checkLaporan(RequestCheckLaporan rLaporan){
        //Check user
        User userCheck = userRepository.findById(rLaporan.getIdChekedBy()).orElseThrow(
                () -> new NotFoundException("Id not found : " + rLaporan.getIdChekedBy())
        );
        if(!userCheck.getRole().getName().equalsIgnoreCase("checker")){
            throw new NotFoundException("Id bukan check");
        }
        LaporanPajak laporanPajak = laporanPajakRepository.findById(rLaporan.getIdLaporan()).orElseThrow(
                () -> new NotFoundException("Id laporan not found : " + rLaporan.getIdLaporan())
        );
        LaporanPajak newLaporan = new LaporanPajak();
        newLaporan.setUserByMadeBy(laporanPajak.getUserByMadeBy());
        newLaporan.setNomorResi(laporanPajak.getNomorResi());
        newLaporan.setStatus(rLaporan.getStatus());
        newLaporan.setUserByApprovedBy(new User(rLaporan.getIdChekedBy()));
        newLaporan.setUserByCheckedBy(new User(rLaporan.getIdChekedBy()));
        newLaporan.setCreatedDate(laporanPajak.getCreatedDate());
        newLaporan.setIdPelaporan(laporanPajak.getIdPelaporan());
        laporanPajak = laporanPajakRepository.save(newLaporan);
        return modelMapper.map(laporanPajak, LaporanPajakDto.class);
    }

    public List<LaporanPajakDto> getSubmitLaporan(long id){
        User user = userRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Id not found : " + id)
        );
        List<LaporanPajak> laporanPajakList = new ArrayList<>();
        if(user.getRole().getName().equalsIgnoreCase("checker")){
             laporanPajakList = laporanPajakRepository.findByMadeByMaker(1);
        } else if (user.getRole().getName().equalsIgnoreCase("approver")) {
            laporanPajakList = laporanPajakRepository.findByStatus("approved");
        }else {
//            throw () -> new exception();/
        }
        List<LaporanPajakDto> laporanPajakDtos = new ArrayList<>();
        for (LaporanPajak laporan :
                laporanPajakList) {
            LaporanPajakDto laporanPajakDto = modelMapper.map(laporan,LaporanPajakDto.class);
            laporanPajakDtos.add(laporanPajakDto);

        }
        return  laporanPajakDtos;
    }
}
