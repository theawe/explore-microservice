package com.example.applaporanpajak.dtos;


public class UserDto {

    private long idUser;
    private RoleDto role;
    private String email;
    private String password;

    public UserDto() {
    }

    public UserDto(long idUser, RoleDto role, String email, String password) {
        this.idUser = idUser;
        this.role = role;
        this.email = email;
        this.password = password;
    }

    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }

    public RoleDto getRole() {
        return role;
    }

    public void setRole(RoleDto role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
