package com.example.applaporanpajak.dtos;


public class RoleDto {

    private long idRole;
    private RoleTypeDto roleType;
    private String name;

    public RoleDto() {
    }

    public RoleDto(long idRole, RoleTypeDto roleType, String name) {
        this.idRole = idRole;
        this.roleType = roleType;
        this.name = name;
    }

    public long getIdRole() {
        return idRole;
    }

    public void setIdRole(long idRole) {
        this.idRole = idRole;
    }

    public RoleTypeDto getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleTypeDto roleType) {
        this.roleType = roleType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
