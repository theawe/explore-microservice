package com.example.applaporanpajak.dtos;

public class RequestCheckLaporan {
    private long idLaporan;
    private String status;
    private long idChekedBy;

    public RequestCheckLaporan() {
    }

    public RequestCheckLaporan(long idLaporan, String status, long idChekedBy) {
        this.idLaporan = idLaporan;
        this.status = status;
        this.idChekedBy = idChekedBy;
    }

    public long getIdLaporan() {
        return idLaporan;
    }

    public void setIdLaporan(long idLaporan) {
        this.idLaporan = idLaporan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getIdChekedBy() {
        return idChekedBy;
    }

    public void setIdChekedBy(long idChekedBy) {
        this.idChekedBy = idChekedBy;
    }
}
