package com.example.applaporanpajak.dtos;

import com.example.applaporanpajak.models.User;

import java.util.Date;

public class RequestCreateLaporan {

    private long idPelaporan;
    private Long idCheckedBy;
    private Long idMadeBy;
    private Long idApprovedBy;
    private long nomorResi;
    private String status;
    private Date createdDate;

    public RequestCreateLaporan() {
    }

    public RequestCreateLaporan(long idPelaporan, Long idCheckedBy, Long idMadeBy, Long idApprovedBy, long nomorResi, String status, Date createdDate) {
        this.idPelaporan = idPelaporan;
        this.idCheckedBy = idCheckedBy;
        this.idMadeBy = idMadeBy;
        this.idApprovedBy = idApprovedBy;
        this.nomorResi = nomorResi;
        this.status = status;
        this.createdDate = createdDate;
    }

    public long getIdPelaporan() {
        return idPelaporan;
    }

    public void setIdPelaporan(long idPelaporan) {
        this.idPelaporan = idPelaporan;
    }

    public Long getIdCheckedBy() {
        return idCheckedBy;
    }

    public void setIdCheckedBy(Long idCheckedBy) {
        this.idCheckedBy = idCheckedBy;
    }

    public Long getIdMadeBy() {
        return idMadeBy;
    }

    public void setIdMadeBy(Long idMadeBy) {
        this.idMadeBy = idMadeBy;
    }

    public Long getIdApprovedBy() {
        return idApprovedBy;
    }

    public void setIdApprovedBy(Long idApprovedBy) {
        this.idApprovedBy = idApprovedBy;
    }

    public long getNomorResi() {
        return nomorResi;
    }

    public void setNomorResi(long nomorResi) {
        this.nomorResi = nomorResi;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
