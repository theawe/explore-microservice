package com.example.applaporanpajak.dtos;


public class LaporanPajakDto {
    private long idPelaporan;
    private UserDto userByCheckedBy;
    private UserDto userByMadeBy;
    private UserDto userByApprovedBy;
    private long nomorResi;
    private String status;

    public LaporanPajakDto() {
    }

    public LaporanPajakDto(long idPelaporan, UserDto userByCheckedBy, UserDto userByMadeBy, UserDto userByApprovedBy, long nomorResi, String status) {
        this.idPelaporan = idPelaporan;
        this.userByCheckedBy = userByCheckedBy;
        this.userByMadeBy = userByMadeBy;
        this.userByApprovedBy = userByApprovedBy;
        this.nomorResi = nomorResi;
        this.status = status;
    }

    public long getIdPelaporan() {
        return idPelaporan;
    }

    public void setIdPelaporan(long idPelaporan) {
        this.idPelaporan = idPelaporan;
    }

    public UserDto getUserByCheckedBy() {
        return userByCheckedBy;
    }

    public void setUserByCheckedBy(UserDto userByCheckedBy) {
        this.userByCheckedBy = userByCheckedBy;
    }

    public UserDto getUserByMadeBy() {
        return userByMadeBy;
    }

    public void setUserByMadeBy(UserDto userByMadeBy) {
        this.userByMadeBy = userByMadeBy;
    }

    public UserDto getUserByApprovedBy() {
        return userByApprovedBy;
    }

    public void setUserByApprovedBy(UserDto userByApprovedBy) {
        this.userByApprovedBy = userByApprovedBy;
    }

    public long getNomorResi() {
        return nomorResi;
    }

    public void setNomorResi(long nomorResi) {
        this.nomorResi = nomorResi;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
