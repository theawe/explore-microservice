package com.example.applaporanpajak.repository;

import com.example.applaporanpajak.models.LaporanPajak;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LaporanPajakRepository extends JpaRepository<LaporanPajak,Long> {

    @Query("SELECT a FROM LaporanPajak a WHERE a.userByMadeBy.role.idRole = ?1")
    List<LaporanPajak> findByMadeByMaker(long idRole);

//    @Query("SELECT a FROM LaporanPajak a WHERE a.status = ?1")
//    List<LaporanPajak> findByStatus(String status);

    List<LaporanPajak> findByStatus(String status);
}
