package com.example.applaporanpajak.controller;

import com.example.applaporanpajak.dtos.RequestCheckLaporan;
import com.example.applaporanpajak.dtos.RequestCreateLaporan;
import com.example.applaporanpajak.service.LaporanPajakService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/laporan")
public class LaporanPajakController {

    @Autowired
    LaporanPajakService laporanPajakService;

    @PostMapping("/create")
    public ResponseEntity<Object> createLaporan(@RequestBody RequestCreateLaporan rLaporan){
        return new ResponseEntity<>(laporanPajakService.createLaporan(rLaporan), HttpStatus.CREATED);
    }

    @PostMapping("/check")
    public ResponseEntity<Object> checkLaporan(@RequestBody RequestCheckLaporan rLaporan){

        return new ResponseEntity<>(laporanPajakService.checkLaporan(rLaporan),HttpStatus.OK);
    }

    @GetMapping("/search")
    public ResponseEntity<Object> searchLaporan(@RequestParam(value = "idUser")long idUser){
        return new ResponseEntity<>(laporanPajakService.getSubmitLaporan(idUser),HttpStatus.OK);
    }
}
